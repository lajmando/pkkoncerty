﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace PKKoncerty.Repository
{
    public class MongoRepository : IMongoRepository
    {
        private readonly IMongoDatabase _database;

        public MongoRepository(IMongoClient client)
        {
            _database = client.GetDatabase("PKKoncerty");
        }

        public void Insert<T>(string document, T record)
        {
            var collection = _database.GetCollection<T>(document);
            
            collection.InsertOne(record);
        }

        public void InsertMany<T>(string document, IEnumerable<T> records)
        {
            var collection = _database.GetCollection<T>(document);

            collection.InsertMany(records);
        }

        public void Delete<T>(string document, Guid id)
        {
            var collection = _database.GetCollection<T>(document);
            var filter = Builders<T>.Filter.Eq("Id", id);

            collection.DeleteOne(filter);
        }

        public void Upsert<T>(string document, Guid id, T record)
        {
            var binData = new BsonBinaryData(id, GuidRepresentation.Standard);
            var collection = _database.GetCollection<T>(document);

            var result = collection.ReplaceOne(
                new BsonDocument("_id", binData),
                record,
                new ReplaceOptions { IsUpsert = true });
        }

        public List<T> Load<T>(string document)
        {
            var collection = _database.GetCollection<T>(document);

            return collection.Find(new BsonDocument()).ToList();
        }
    }
}
