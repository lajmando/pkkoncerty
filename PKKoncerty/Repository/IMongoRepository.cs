﻿using System;
using System.Collections.Generic;

namespace PKKoncerty.Repository
{
    public interface IMongoRepository
    {
        void Insert<T>(string document, T record);
        void InsertMany<T>(string document, IEnumerable<T> records);
        void Upsert<T>(string table, Guid id, T record);
        void Delete<T>(string document, Guid id);
        List<T> Load<T>(string document);
    }
}
