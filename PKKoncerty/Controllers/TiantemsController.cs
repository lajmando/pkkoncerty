﻿using Microsoft.AspNetCore.Mvc;
using PKKoncerty.Models;
using PKKoncerty.Repository;
using System;
using System.Collections.Generic;

namespace PKKoncerty.Controllers
{
    [Route("api/tiantems")]
    [ApiController]
    public class TiantemsController : ControllerBase
    {
        private readonly IMongoRepository _repository;

        public TiantemsController(IMongoRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("get")]
        public IEnumerable<Tiatem> Get()
        {
            var collections = _repository.Load<Tiatem>("tiatems");

            return collections;
        }

        [HttpPost("insert")]
        public void Insert(Tiatem tiatem)
        {
            _repository.Insert("tiatems", tiatem);
        }

        [HttpPut("update")]
        public void Update(Guid id, Tiatem tiatem)
        {
            _repository.Upsert("tiatems", id, tiatem);
        }

        [HttpPost("seed-data-tiatems")]
        public void Seed()
        {
            var tiatems = new List<Tiatem>()
            {
                new Tiatem() { 
                    TantiemValue = 7000, 
                    Band = new Band()
                    {
                        Name = "Pieśni i tańca",
                        Members = new List<Member>
                        {
                            new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                            new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                            new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                        }
                    },
                    Concert = new Concert()
                        {
                        Attendance = 55000,
                        Date = DateTime.Now,
                        Bands = new List<Band>()
                        {
                            new Band()
                            {
                                Name = "Pieśni i tańca",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                    new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                    new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Ich dwoje",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                    new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Stand up Jacek",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                                }
                            }
                        },
                        Songs = new List<Song>()
                        {
                            new Song
                            {
                                Title = "Zatańczmy jeszcze raz",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Miała matka syna",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zostańmy razem",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zyli długo i szczęsliwie",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            }
                        }
                    }
                },
                new Tiatem() {
                    TantiemValue = 4500,
                    Band = new Band()
                    {
                        Name = "Ich dwoje",
                        Members = new List<Member>
                        {
                            new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                            new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                        }
                    },
                    Concert = new Concert()
                        {
                        Attendance = 55000,
                        Date = DateTime.Now,
                        Bands = new List<Band>()
                        {
                            new Band()
                            {
                                Name = "Pieśni i tańca",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                    new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                    new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Ich dwoje",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                    new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Stand up Jacek",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                                }
                            }
                        },
                        Songs = new List<Song>()
                        {
                            new Song
                            {
                                Title = "Zatańczmy jeszcze raz",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Miała matka syna",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zostańmy razem",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zyli długo i szczęsliwie",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            }
                        }
                    }
                },
                new Tiatem() {
                    TantiemValue = 2000,
                    Band = new Band()
                    {
                        Name = "Stand up Jacek",
                        Members = new List<Member>
                        {
                            new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                        }
                    },
                    Concert = new Concert()
                        {
                        Attendance = 55000,
                        Date = DateTime.Now,
                        Bands = new List<Band>()
                        {
                            new Band()
                            {
                                Name = "Pieśni i tańca",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                    new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                    new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Ich dwoje",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                    new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                }
                            },
                            new Band()
                            {
                                Name = "Stand up Jacek",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                                }
                            }
                        },
                        Songs = new List<Song>()
                        {
                            new Song
                            {
                                Title = "Zatańczmy jeszcze raz",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Miała matka syna",
                                Band = new Band()
                                {
                                    Name = "Pieśni i tańca",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zostańmy razem",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            },
                            new Song
                            {
                                Title = "Zyli długo i szczęsliwie",
                                Band = new Band()
                                {
                                    Name = "Ich dwoje",
                                    Members = new List<Member>
                                    {
                                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            _repository.InsertMany("tiatems", tiatems);
        }
    }
}
