﻿using Microsoft.AspNetCore.Mvc;
using PKKoncerty.Models;
using PKKoncerty.Repository;
using System;
using System.Collections.Generic;

namespace PKKoncerty.Controllers
{
    [ApiController]
    [Route("api/bands")]
    public class BandsController : ControllerBase
    {
        private readonly IMongoRepository _repository;

        public BandsController(IMongoRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("get")]
        public IEnumerable<Band> Get()
        {
            var collections = _repository.Load<Band>("bands");

            return collections;
        }

        [HttpPost("insert")]
        public void Insert(Band band)
        {
            _repository.Insert("bands", band);
        }

        [HttpPut("update")]
        public void Update(Guid id, Band band)
        {
            _repository.Upsert("bands", id, band);
        }

        [HttpPost("seed-data-bands")]
        public void Seed()
        {
            var bands = new List<Band>() 
            { 
                new Band()
                {
                    Name = "Pieśni i tańca",
                    Members = new List<Member>
                    {
                        new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                        new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                        new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                    }
                },
                new Band()
                {
                    Name = "Ich dwoje",
                    Members = new List<Member>
                    {
                        new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                        new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                    }
                },
                new Band()
                {
                    Name = "Stand up Jacek",
                    Members = new List<Member>
                    {
                        new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                    }
                }
            };

            _repository.InsertMany("bands", bands);
        }
    }
}
