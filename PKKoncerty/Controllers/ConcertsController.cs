﻿using Microsoft.AspNetCore.Mvc;
using PKKoncerty.Models;
using PKKoncerty.Repository;
using System;
using System.Collections.Generic;

namespace PKKoncerty.Controllers
{
    [Route("api/concerts")]
    [ApiController]
    public class ConcertsController : ControllerBase
    {
        private readonly IMongoRepository _repository;

        public ConcertsController(IMongoRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("get")]
        public IEnumerable<Concert> Get()
        {
            var collections = _repository.Load<Concert>("concerts");

            return collections;
        }

        [HttpPost("insert")]
        public void Insert(Concert band)
        {
            _repository.Insert("concerts", band);
        }

        [HttpPut("update")]
        public void Update(Guid id, Concert concert)
        {
            _repository.Upsert("concerts", id, concert);
        }

        [HttpPost("seed-data-concerts")]
        public void Seed()
        {
            var concerts = new List<Concert>()
            {
                new Concert()
                {
                    Attendance = 55000,
                    Date = DateTime.Now,
                    Bands = new List<Band>()
                    {
                        new Band()
                        {
                            Name = "Pieśni i tańca",
                            Members = new List<Member>
                            {
                                new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                            }
                        },
                        new Band()
                        {
                            Name = "Ich dwoje",
                            Members = new List<Member>
                            {
                                new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                            }
                        },
                        new Band()
                        {
                            Name = "Stand up Jacek",
                            Members = new List<Member>
                            {
                                new Member() { Name = "Jacek", Surname = "Trzycki", Role = "Komik" }
                            }
                        }
                    },
                    Songs = new List<Song>()
                    {
                        new Song
                        {
                            Title = "Zatańczmy jeszcze raz",
                            Band = new Band()
                            {
                                Name = "Pieśni i tańca",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                    new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                    new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                }
                            }
                        },
                        new Song
                        {
                            Title = "Miała matka syna",
                            Band = new Band()
                            {
                                Name = "Pieśni i tańca",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Jan", Surname = "Nowak", Role = "Wokalista" },
                                    new Member() { Name = "Adam", Surname = "Krzyż", Role = "Perkusista" },
                                    new Member() { Name = "Emil", Surname = "Roland", Role = "Gitarzysta" }
                                }
                            }
                        },
                        new Song
                        {
                            Title = "Zostańmy razem",
                            Band = new Band()
                            {
                                Name = "Ich dwoje",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                    new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                }
                            }
                        },
                        new Song
                        {
                            Title = "Zyli długo i szczęsliwie",
                            Band = new Band()
                            {
                                Name = "Ich dwoje",
                                Members = new List<Member>
                                {
                                    new Member() { Name = "Emila", Surname = "Runak", Role = "Wokalista" },
                                    new Member() { Name = "Jerzy", Surname = "Brzęczek", Role = "Gitarzysta" }
                                }
                            }
                        }
                    }
                }
            };

            _repository.InsertMany("concerts", concerts);
        }
    }
}
