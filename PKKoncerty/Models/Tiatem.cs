﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PKKoncerty.Models
{
    public class Tiatem
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("value")]
        public double TantiemValue { get; set; }

        [BsonElement("band")]
        public Band Band { get; set; }

        [BsonElement("concert")]
        public Concert Concert { get; set; }
    }
}
