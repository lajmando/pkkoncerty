﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace PKKoncerty.Models
{
    public class Band
    {
        [BsonId]
        public ObjectId Id { get; set; }
        
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("members")]
        public IEnumerable<Member> Members { get; set; }
    }
}
