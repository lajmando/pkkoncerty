﻿using MongoDB.Bson.Serialization.Attributes;

namespace PKKoncerty.Models
{
    public class Song
    {
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("band")]
        public Band Band { get; set; }
    }
}