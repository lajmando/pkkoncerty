﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace PKKoncerty.Models
{
    public class Concert
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonElement("bands")]
        public IEnumerable<Band> Bands { get; set; }

        [BsonElement("attendance")]
        public int Attendance { get; set; }

        [BsonElement("songs")]
        public IEnumerable<Song> Songs { get; set; }
    }
}
