﻿using MongoDB.Bson.Serialization.Attributes;

namespace PKKoncerty.Models
{
    public class Member
    {
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("surname")]
        public string Surname { get; set; }

        [BsonElement("role")]
        public string Role { get; set; }
    }
}
